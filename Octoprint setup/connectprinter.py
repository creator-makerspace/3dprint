#!/usr/bin/python

OCTOPRINTCONN_URL = 'http://localhost:5000/api/connection'
OCTOPRINTPRINT_URL = 'http://localhost:5000/api/printer'
API_KEY = 'D80FC91F0B224FB288E4576C22052195'
BAUDRATE = 115200


import requests
import sys

#port = sys.argv[1]
headers = {'X-Api-Key': API_KEY}
json = {
  "exclude": "temprature,sd",
  "baudrate": BAUDRATE,
}
r = requests.get(
        OCTOPRINTPRINT_URL,
        json=json,
        headers=headers
)
if (r.status_code == 200):
    sys.exit(0)
else:
    json = {
          "command": "connect",
    }
    r = requests.post(
        OCTOPRINTCONN_URL,
        json=json,
        headers=headers
    )
    print "Connecting to printer"