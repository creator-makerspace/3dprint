axes:
  e:
    inverted: false
    speed: 7200
  x:
    inverted: false
    speed: 10200
  y:
    inverted: false
    speed: 10200
  z:
    inverted: false
    speed: 720
color: default
extruder:
  count: 1
  nozzleDiameter: 0.4
  offsets:
  - - 0.0
    - 0.0
  sharedNozzle: false
heatedBed: true
heatedChamber: false
id: _default
model: Prusa i3 MK3
name: Prusa i3 MK3
volume:
  custom_box: false
  depth: 210.0
  formFactor: rectangular
  height: 210.0
  origin: lowerleft
  width: 250.0
