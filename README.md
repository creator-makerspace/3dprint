## WHAT
This repo contains the most up-to-date version of the slicer profiles used at Creator. It also is the home for the `cfg` files used on the printers. 

## Printers 
A total of 8 printers is tracked in this repo:
- Prusa 1 -- [prusa1.local](prusa1.local)
- Prusa 2 -- [prusa2.local](prusa2.local)
- Prusa 3 -- [prusa3.local](prusa3.local)
- Prusa 4 -- [prusa4.local](prusa4.local)
- Prusa 5 -- [prusa5.local](prusa5.local)
- Prusa 6 -- [prusa6.local](prusa6.local)
- Prusa 7 -- [prusa7.local](prusa7.local)
- Prusa 8 -- [prusa8.local](prusa8.local)

## General printer setup
Prusa MK3/s running [Klipper](https://www.klipper3d.org/) firmware. Otherwise running with stock hardware. Hardlimited to 300 mm/s and 6000 mm/s^2 
